Glose challenge app
===================

![screen](screen.png)

## Achieved

#### Glose front page clone

- responsive
- isomorphic
- super easy to setup, build and serve

## How to

```bash
git clone https://bitbucket.org/elbywan/gloseapp.git
cd gloseapp
npm install     # install dependencies
npm run dev     # launch webpack dev server (http://localhost:8080)
npm start       # build universal app
npm run serve   # serves the universal app - build it first ! (http://localhost:8080)
```

## Tools

- Typescript
- React / redux
- Express for the universal rendering
- Webpack for the bundling
- Postcss + nextcss + nanocss alongside webpack loaders to split and import css, then render it in style tags or a separate file (dev/prod build)
- Handlebars to inject the preloaded store, preprocessed html and css and js dependencies easily