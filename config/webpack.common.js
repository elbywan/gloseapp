const { resolve } = require("path")

const src = (file = "") => resolve(__dirname, "..", "src", file)
const build = (file = "") => resolve(__dirname, "..", "build", file)

module.exports = {
    output: {
        path: build(),
        filename: "[name].min.js"
    },
    resolve: {
        extensions: [ ".js", ".jsx", ".ts", ".tsx" ],
        alias: {
            self: src()
        }
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                options: {
                    configFile: resolve(__dirname, "..", "tsconfig.json"),
                    compilerOptions: { declaration: false }
                }
            },
            {
                test: /\.hbs/,
                loader: "handlebars-loader"
            }
        ]
    }
}
