const { resolve } = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const ExtractTextPlugin = require("extract-text-webpack-plugin")

const commonConfig = require("./webpack.common")

const src = (file = "") => resolve(__dirname, "..", "src", file)
const build = (file = "") => resolve(__dirname, "..", "build", file)

module.exports = {
    ...commonConfig,
    entry: {
        client: src("index.tsx"),
    },
    output: {
        ...commonConfig.output,
        path: build("static")
    },
    module: {
        rules: [
            ...commonConfig.module.rules,
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract([
                    "css-loader",
                    "postcss-loader"
                ])
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin("style.css"),
        new HtmlWebpackPlugin({
            filename: src("index.html"),
            template: src("index.hbs"),
            inject: true
        })
    ]
}
