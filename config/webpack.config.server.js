const { resolve } = require("path")
const ExtractTextPlugin = require("extract-text-webpack-plugin")

const commonConfig = require("./webpack.common")

const src = (file = "") => resolve(__dirname, "..", "src", file)
const build = (file = "") => resolve(__dirname, "..", "build", file)

module.exports = {
    ...commonConfig,
    entry: {
        server: src("server/index.tsx")
    },
    module: {
        rules: [
            ...commonConfig.module.rules,
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract([
                    "css-loader",
                    "postcss-loader"
                ])
            }
        ]
    },
    target: "node",
    node: {
        __dirname: false,
        __filename: false
    },
    plugins: [
        new ExtractTextPlugin("static/style.css")
    ]
}
