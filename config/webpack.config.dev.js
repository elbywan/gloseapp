const { resolve } = require("path")
const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")

const src = (file = "") => resolve(__dirname, "..", "src", file)
const build = (file = "") => resolve(__dirname, "..", "build", file)

module.exports = {
    entry: {
        hotreload: "webpack/hot/only-dev-server",
        glose: [
            "react-hot-loader/patch",
            src("index.tsx")
        ]
    },
    output: {
        path: build(),
        filename: "[name].min.js",
        publicPath: "/"
    },
    resolve: {
        extensions: [ ".js", ".jsx", ".ts", ".tsx" ],
        alias: {
            self: src()
        }
    },
    devtool: "inline-source-map",
    devServer: {
        hot: true,
        host: "0.0.0.0",
        disableHostCheck: true,
        contentBase: build(),
        publicPath: "/",
        port: 8080,
        compress: true
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    { loader: "react-hot-loader/webpack" },
                    {
                        loader: "ts-loader",
                        options: {
                            configFile: resolve(__dirname, "..", "tsconfig.json"),
                            transpileOnly: true
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader", options: { importLoaders: 1 }},
                    "postcss-loader"
                ]
            },
            {
                test: /\.hbs/,
                loader: "handlebars-loader"
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new HtmlWebpackPlugin({
            template: src("index.hbs"),
            inject: true
        })
    ]
}
