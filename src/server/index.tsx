import * as path from "path"
import * as express from "express"
import * as React from "react"
import { renderToString } from "react-dom/server"
import { createStore, applyMiddleware } from "redux"
import { Provider } from "react-redux"
import thunk from "redux-thunk"
import { readFileSync } from "fs"
import { resolve } from "path"
import wretch from "wretch"

import rootReducer from "self/store/reducers"
import { App } from "self/components"
import * as actions from "self/store/actions"
// Style is exported by webpack to a separate css file
import "../style/style.css"

// Handlebars index template
const indexTemplate = require("../index.hbs")

// Isomorphic fetch polyfill
wretch().polyfills({ fetch: require("node-fetch") })

const app = express()
const port = 8080

// Static css & js
app.use("/static", express.static(resolve(__dirname, "static")))
// Universal handler
app.use((req, res) => {
    const store = createStore(rootReducer, applyMiddleware(thunk))
    // Prefetch the books
    store.dispatch(actions.fetchBooks()).then(() => {
        // Render react server-side
        const html = renderToString(<Provider store={ store }><App /></Provider>)
        const preloadedState = store.getState()
        // Process the handlebars template and inject stuff
        const renderedHtml = indexTemplate({
            preloadedState: JSON.stringify(preloadedState).replace(/</g, "\\u003c"),
            html: html,
            bundles: [ "/static/client.min.js" ],
            styles: [ "/static/style.css" ]
        })

        res.send(renderedHtml)
    })
})

app.listen(port)