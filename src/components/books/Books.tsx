import * as React from "react"
import { Book, Spinner } from "self/components"

import "./Books.css"

export const Books = ({ books }) =>
    books.isFetching ?
        <Spinner/> :
    books.error ?
        <div className="error-band">Erreur ({books.error.status}) : { books.error.json.error }</div> :
    !books.items || !books.items.length ?
        <div className="books"><h2>Aucun livre.</h2></div> :
    <div className="books">{ books.items.map(book => <Book key={ book.id } { ...book } />) }</div>