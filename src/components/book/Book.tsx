import * as React from "react"
import { RoundLetter } from "self/components"
import "./Book.css"

const renderAuthors = authors => !authors || !authors.length ? null :
    <span className="book-authors ellipsis">
        par { authors.map((_, idx) => <strong key={ _.slug }>{ idx > 1 ? ", ": "" }{ _.name }</strong>) }
    </span>

export const Book = ({ image, title, authors }) =>
    <div className="book">
        <img src={ image }/>
        <div className="book-bar">
            <RoundLetter/><RoundLetter/><RoundLetter/>
            <button className="action">Lire</button>
        </div>
        <div className="book-text">
            <a className="book-title ellipsis">{ title }</a>
            { renderAuthors(authors) }
        </div>

    </div>