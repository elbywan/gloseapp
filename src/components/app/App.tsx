import * as React from "react"
import { connect, Dispatch } from "react-redux"
import { Books } from "self/components"
import * as actions from "self/store/actions"
import wretch from "wretch"

class _App extends React.Component<{ dispatch: any, books: { items: Array<any> } }> {

    componentDidMount() {
        // When rendered server-side the store is already populated
        if(!this.props.books.items.length)
            this.props.dispatch(actions.fetchBooks())
    }

    render() {
        return (
            <div>
                <header><h2>Glose lookalike</h2></header>
                <Books books={ this.props.books }/>
            </div>
        )
    }
}

const mapStateToProps = ({ books }) => ({ books })

export const App = connect(mapStateToProps)(_App)