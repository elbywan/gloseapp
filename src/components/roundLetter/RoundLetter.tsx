import * as React from "react"
import "./RoundLetter.css"

// A randomized pretty letter to simulate users

const randomLetter = () => String.fromCharCode((26 * Math.random() | 0) + 65)
const randomColor = () => {
    const color = ((1 << 24) * Math.random() & 0xFEFEFE | 0xCCCCCC).toString(16)
    const pad = (str, length) => str.length < length ? pad("0" + str, length) : str
    return "#" + pad(color, 6)
}

export const RoundLetter = () =>
    <span className="round-letter" style={{ background: randomColor() }}>{ randomLetter() }</span>