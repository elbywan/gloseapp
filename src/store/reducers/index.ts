import * as actions from "self/store/actions"

const reduceBooks = (books = { isFetching: false, error: undefined, items: [] }, action) => {
    switch(action.type) {
        case actions.REQUEST_BOOKS:
            return { ...books, error: undefined, isFetching: true }
        case actions.RECEIVE_BOOKS:
            return { error: undefined, isFetching: false, items: action.books }
        case actions.ERROR_BOOKS:
            return { error: action.error, isFetching: false, items: [] }
        default:
            return books
    }
}

export default (state = { books: undefined }, action) => ({
    books: reduceBooks(state.books, action)
})