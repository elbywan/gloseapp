import { booksApi } from "self/api"

export const REQUEST_BOOKS = "REQUEST_BOOKS"
export const RECEIVE_BOOKS = "RECEIVE_BOOKS"
export const ERROR_BOOKS   = "ERROR_BOOKS"

const requestBooks = () => {
    return {
        type: REQUEST_BOOKS
    }
}

const receiveBooks = json => {
    const books = json ?
        json.modules.find(_ => _.type === "books-vertical").books :
        null
    return {
        type: RECEIVE_BOOKS,
        books: books
    }
}

const errorBooks = error => {
    return {
        type: ERROR_BOOKS,
        error: error
    }
}

export const fetchBooks = () => dispatch => {
    dispatch(requestBooks())
    return booksApi
        .get()
        .json(json => dispatch(receiveBooks(json)))
        .catch(error => dispatch(errorBooks(error)))
}