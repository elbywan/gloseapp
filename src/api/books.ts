import wretch from "wretch"
import { ResponseChain } from "wretch/dist/resolver"

const w = wretch("https://api.glose.com/v1/booklists/").errorType("json")

export const booksApi = {
    get: () => w.url("free-books").get()
}